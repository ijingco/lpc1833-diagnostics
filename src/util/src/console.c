#include "stdio.h"
#include "lpc_types.h"
#include "string.h"
#include "lpc18xx_scu.h"
#include "lpc18xx_uart.h"

#define USED_UART_PORT    2

#if (USED_UART_PORT==0)
#define UART_PORT    LPC_USART0
#elif (USED_UART_PORT==1)
#define UART_PORT    LPC_UART1
#elif (USED_UART_PORT==2)
#define UART_PORT    LPC_USART2
#endif

/*********************************************************************//**
 * @brief        Console Initialization
 * @param[in]    none
 * @return         none
 **********************************************************************/
void ConsoleInit(void)
{
    UART_CFG_Type UARTConfigStruct;

#if (USED_UART_PORT==0)
    /*
     * Initialize UART0 pin connect
     */
    scu_pinmux(0xF ,10 , MD_PDN, FUNC1);     // PF.10 : UART0_TXD
    scu_pinmux(0xF ,11 , MD_PLN|MD_EZI|MD_ZI, FUNC1);     // PF.11 : UART0_RXD
#elif (USED_UART_PORT==1)
    /*
     * Initialize UART1 pin connect
     */
    scu_pinmux(0xC ,13 , MD_PDN, FUNC2);     // PC.13 : UART1_TXD
    scu_pinmux(0xC ,14 , MD_PLN|MD_EZI|MD_ZI, FUNC2);     // PC.14 : UART1_RXD
#elif (USED_UART_PORT==2)
    /*
     * Initialize UART2 pin connect
     */
    scu_pinmux(0x1 ,15 , MD_PDN, FUNC1);     // P1.15 : U2_TXD K8
    scu_pinmux(0x1 ,16 , MD_PLN|MD_EZI|MD_ZI, FUNC1);     // P1.16 : U2_RXD H9
#endif

    /* Initialize UART Configuration parameter structure to default state:
     * Baudrate = 9600bps
     * 8 data bit
     * 1 Stop bit
     * None parity
     */
    UART_ConfigStructInit(&UARTConfigStruct);
    // Re-configure baudrate to 115200bps
    UARTConfigStruct.Baud_rate = 115200;

    // Initialize UART_PORT peripheral with given to corresponding parameter
    UART_Init((LPC_USARTn_Type*)UART_PORT, &UARTConfigStruct);

    // Enable UART Transmit
    UART_TxCmd((LPC_USARTn_Type*)UART_PORT, ENABLE);
}

/*********************************************************************//**
 * @brief        Print menu item text
 * @param[in]    cmd - character command of menu item
 * @param[in]    str - description of menu item
 * @return         none
 **********************************************************************/
void printMenuItem(uint8_t cmd,uint8_t str[30])
{
    uint8_t buff[40];
    sprintf((char*)buff, "    %c - %s\r\n", cmd, str);
    UART_Send(UART_PORT, buff, strlen((char*)buff), BLOCKING);
}

/*********************************************************************//**
 * @brief        Print string
 * @param[in]    str - string to print
 * @return         none
 **********************************************************************/
void printString(uint8_t str[50])
{
    uint8_t buff[60];
    sprintf((char*)buff, "%s\r\n", str);
    UART_Send(UART_PORT, buff, strlen((char*)buff), BLOCKING);
}

/*********************************************************************//**
 * @brief        Read input
 * @param[in]    none
 * @return         none
 **********************************************************************/
uint8_t readInput(void)
{
    uint8_t rev_buf;          // Reception buffer
    
    /* Read the received data */
    while(!(UART_Receive(UART_PORT, &rev_buf, 1, NONE_BLOCKING) == 1));
    // Echo characters received
    UART_Send(UART_PORT, &rev_buf, 1, NONE_BLOCKING);

    return (rev_buf);
}

/*********************************************************************//**
 * @brief        Read string
 * @param[in]    maxLnt - maximum lengt of string to read
 * @param[in]    buff - buffer for read string
 * @return         none
 **********************************************************************/
uint8_t readString(uint8_t maxLnt, uint8_t *buff)
{
    uint8_t    rev_buf[255],          // Reception buffer
                    lnt, tmp;
    
    memset(buff, '\0', sizeof(buff));
    
    /* Read the received data */
    for(lnt = 0; lnt < maxLnt; lnt++)
    {
        tmp = readInput();
        if(tmp == 13)
            break;
        rev_buf[lnt] = tmp;
    }
    memcpy(buff, rev_buf, lnt);
    
    return lnt;
}
