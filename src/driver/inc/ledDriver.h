#define LED_RED		0
#define LED_GREEN	1
#define LED_BLUE	2

#define LED_ON	1
#define LED_OFF	0

void LedInit(void);
void LedCtl(uint8_t sts, uint8_t led);
