
#define ACC_ADR_DEVID                 0
//Reserved                            1~28
#define ACC_ADR_THRESH_TAP            29
#define ACC_ADR_OFSX                  30
#define ACC_ADR_OFSY                  31
#define ACC_ADR_OFSZ                  32
#define ACC_ADR_DUR                   33
#define ACC_ADR_LATENT                34
#define ACC_ADR_WINDOW                35
#define ACC_ADR_THRESH_ACT            36
#define ACC_ADR_THRESH_INACT          37
#define ACC_ADR_TIME_INACT            38
#define ACC_ADR_ACT_INACT_CTL         38
#define ACC_ADR_THRESH_FF             40
#define ACC_ADR_TIME_FF               41
#define ACC_ADR_TAP_AXES              42
#define ACC_ADR_ACT_TAP_STATUS    	  43
#define ACC_ADR_BW_RATE               44
#define ACC_ADR_POWER_CTL             45
#define ACC_ADR_INT_ENABLE            46
#define ACC_ADR_INT_MAP               47
#define ACC_ADR_INT_SOURCE            48
#define ACC_ADR_DATA_FORMAT           49
#define ACC_ADR_DATAX0                50
#define ACC_ADR_DATAX1                51
#define ACC_ADR_DATAY0                52
#define ACC_ADR_DATAY1                53
#define ACC_ADR_DATAZ0                54
#define ACC_ADR_DATAZ1                55
#define ACC_ADR_FIFO_CTL              56
#define ACC_ADR_FIFO_STATUS           57

#define ACC_MODE_MEASURE              0x08
#define ACC_MODE_STANDBY              0x00

#define ACC_RANGE_2G                  0x00
#define ACC_RANGE_4G                  0x01
#define ACC_RANGE_8G                  0x02
#define ACC_RANGE_16G                 0x03

uint32_t AccRead(uint32_t address);
void AccWrite(uint32_t address, uint32_t data);
void AccInit(void);
void AccSetRange(uint32_t range);
void ReadAccXYZ(int32_t *x, int32_t *y, int32_t *z);
