typedef struct
{
    uint8_t port;
    uint8_t pin;
    uint8_t func;
    uint8_t gpioPort;
    uint32_t gpioBit;
    uint8_t dir;
} ST_GpioPinConfig;

#define INPUT		0
#define OUTPUT	1

#define HIGH		1
#define LOW		0

#define DIGIN1_EN	0 // Digital input 1 enable
#define DIGIN2_EN	1 // Digital input 2 enable
#define DIGIN3_EN	2 // Digital input 3 enable
#define DIGIN1		3 // Digital input 1
#define DIGIN2		4 // Digital input 2
#define DIGIN3		5 // Digital input 3
#define PANIC_LO	6 // Panic input low
#define PANIC_HI	7 // Panic input high
// for confirmation
//#define DIGIOUT		8 // Panic input high

void GpioInit(void);
void GpioCtl(uint8_t gpio, uint8_t sts);
uint32_t GpioRead(uint8_t gpio);
