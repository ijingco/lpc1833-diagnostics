#include "lpc_types.h"
#include "menu.h"
#include "console.h" 

/*********************************************************************//**
 * @brief        Search for menu item
 * @param[in]    menu    - Menu table to be searched
 * @param[in]    key        - Key used for searching
 * @return         position of item to be searched
 *                      - Out of bound if not found
 **********************************************************************/
uint16_t searchMenu(ST_MenuTable* menu, uint8_t key)
{
    uint16_t c = 0;
    ST_MenuItem* items = menu->item;
    
    while(c < menu->cnt)    
    {
        if((items[c].cmd == key)
        ||(items[c].cmd+0x20 == key))
            return c;
        c++;
    }
    return c;
}

/*********************************************************************//**
 * @brief        Run Menu
 * @param[in]    menu    - Menu table to be run
 * @return         none
 **********************************************************************/
void runMenu(ST_MenuTable* menu)
{
    uint8_t running = 1;
    uint16_t c;
    uint16_t index;
    uint8_t input;
    ST_MenuItem* items = menu->item;
    
    
    while(running)
    {
        // Display menu
        printString(menu->str);
        for(c = 0;c < menu->cnt; c++)
            printMenuItem(items[c].cmd,items[c].str);
        
        // wait for user command
        printString("Please sellect a command");
        input = readInput();
        printString("");
        printString("");
        
        // check selected command
        index = searchMenu(menu, input);
        
        // run handler of sellected command
        if(index < menu->cnt)
            running = runMenuItem(&items[index],input);
    }
}

/*********************************************************************//**
 * @brief        Run the selected menu item
 * @param[in]    menu    - Menu item to be run
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t runMenuItem(ST_MenuItem* menu,uint8_t cmd)
{
    if(menu->subMenu != NULL)
        runMenu(menu->subMenu);
    else if(menu->op != NULL)
        return menu->op(cmd);
    return MENU_CONTINUE;
}


uint8_t exitMenu(uint8_t cmd)
{
    return MENU_EXIT;
}
