#include "lpc18xx_gpio.h"
#include "lpc18xx_scu.h"
#include "gpioDriver.h"

ST_GpioPinConfig gpioCfgTbl[] =
{
    //port       pin    func    gpioPort    gpioBit        dir
    {   6,        9,    FUNC0,    3,        5,           OUTPUT},    // DIGIN1_EN
    {   6,        5,    FUNC0,    3,        4,           OUTPUT},    // DIGIN2_EN
    {   6,        4,    FUNC0,    3,        3,           OUTPUT},    // DIGIN3_EN
    {   1,        4,    FUNC0,    0,        11,          INPUT},     // DIGIN1
    {   1,        6,    FUNC0,    1,        9,           INPUT},     // DIGIN2
    {   1,        7,    FUNC0,    1,        0,           INPUT},     // DIGIN3
    {   1,        12,   FUNC0,    1,        5,           INPUT},     // PANIC_LO
    {   2,        8,    FUNC4,    5,        7,            INPUT},    // PANIC_HI
// for confirmation
    //{    0,        0,    FUNC0,    0,                0,                 OUTPUT},    // DIGIOUT
};

#define GPIO_CFG_TBL_COUNT (sizeof(gpioCfgTbl)/sizeof(ST_GpioPinConfig))

/*********************************************************************//**
 * @brief        GPIO pin initialization
 * @param[in]    none
 * @return         none
 **********************************************************************/
void GpioInit(void)
{
    uint8_t c, mode;
    
    for(c = 0; c < GPIO_CFG_TBL_COUNT; c++)
    {
        if(gpioCfgTbl[c].dir == OUTPUT)
            mode = MD_PDN;
        else
            mode = MD_PLN|MD_EZI|MD_ZI;
        scu_pinmux(gpioCfgTbl[c].port, gpioCfgTbl[c].pin, mode, gpioCfgTbl[c].func);
        GPIO_SetDir(gpioCfgTbl[c].gpioPort, gpioCfgTbl[c].gpioBit, gpioCfgTbl[c].dir);
        if(gpioCfgTbl[c].dir == OUTPUT)
            GPIO_ClearValue(gpioCfgTbl[c].gpioPort, gpioCfgTbl[c].gpioBit);
    }
}

/*********************************************************************//**
 * @brief        GPIO control
 * @param[in]    led - GPIO pin to control
 *                         DIGIN1_EN        - Digital input 1 enable
 *                         DIGIN2_EN        - Digital input 2 enable
 *                         DIGIN3_EN        - Digital input 3 enable
 *                         DIGIOUT            - Digital output
 * @param[in]    sts - new state
 *                         GPIO_HIGH    - set gpio
 *                         GPIO_LOW    - clear gpio
 * @return         none
 **********************************************************************/
void GpioCtl(uint8_t gpio, uint8_t sts)
{
    if(sts == HIGH)
    {
        GPIO_SetValue(gpioCfgTbl[gpio].gpioPort, gpioCfgTbl[gpio].gpioBit);
    }
    else
    {
        GPIO_ClearValue(gpioCfgTbl[gpio].gpioPort, gpioCfgTbl[gpio].gpioBit);
    }
}

/*********************************************************************//**
 * @brief        GPIO read
 * @param[in]    led - GPIO pin to read
 *                         DIGIN1        - Digital input 1
 *                         DIGIN2        - Digital input 2
 *                         DIGIN3        - Digital input 3
 *                         PANIC_LO    - Panic input low
 *                         PANIC_HI    - Panic input high
 * @return         value - gpio status
 *                             GPIO_HIGH    - set gpio
 *                             GPIO_LOW    - clear gpio
 **********************************************************************/
uint32_t GpioRead(uint8_t gpio)
{
    uint32_t value;

    value = GPIO_ReadValue(gpioCfgTbl[gpio].gpioPort);
    value &= gpioCfgTbl[gpio].gpioBit;

    return value;
}
