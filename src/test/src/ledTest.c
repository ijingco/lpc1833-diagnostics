#include "main.h"
#include "ledDriver.h"

uint8_t testLed(uint8_t cmd);

typedef struct
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} St_LedRgb;

St_LedRgb colorTable[] =
{
    {LED_OFF,    LED_OFF,    LED_OFF},    //    Off
    {LED_OFF,    LED_OFF,    LED_ON},    //    Blue
    {LED_OFF,    LED_ON,     LED_OFF},    //    Green
    {LED_OFF,    LED_ON,        LED_ON},    //    Cyan
    {LED_ON,    LED_OFF,    LED_OFF},    //    Red
    {LED_ON,    LED_OFF,    LED_ON},    //    Magenta
    {LED_ON,    LED_ON,        LED_OFF},    //    Yellow
    {LED_ON,    LED_ON,        LED_ON}        //    White
};

// Accelerometer diagnostic menu------------------------------//
ST_MenuItem ledMenuItem[] = {
    //    cmd   str         op          subMenu
    {   '1',    "Blue",     testLed,    NULL},
    {   '2',    "Green",    testLed,    NULL},
    {   '3',    "Cyan",     testLed,    NULL},
    {   '4',    "Red",      testLed,    NULL},
    {   '5',    "Magenta",  testLed,    NULL},
    {   '6',    "Yellow",   testLed,    NULL},
    {   '7',    "White",    testLed,    NULL},
    {   'X',    "EXIT",     exitMenu,   NULL}
};

#define LED_MENU_ITEM_COUNT (sizeof(ledMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable ledMenu = {
    "Please sellect LED color",
    ledMenuItem,
    LED_MENU_ITEM_COUNT
};

/*********************************************************************//**
 * @brief        Set LED
 * @param[in]    color        - led color
 * @return         none
 **********************************************************************/
static void setLed(uint8_t color)
{
    LedCtl(LED_RED, colorTable[color].red);
    LedCtl(LED_GREEN, colorTable[color].green);
    LedCtl(LED_BLUE, colorTable[color].blue);
}

/*********************************************************************//**
 * @brief        Test LED
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testLed(uint8_t cmd)
{    
    cmd -= 0x30;
    
    // Set LED to on
    setLed(cmd);
    printString("LED: Led color was set");
    
    // press any key to extit LED test
    printString("LED: Press any key to continue");
    readInput();

    // Set LED to off
    setLed(0);

    return MENU_CONTINUE;
}
