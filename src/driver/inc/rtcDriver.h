#define RTC_ADR_CTR1            0x00
#define RTC_ADR_CTR2            0x01
#define RTC_ADR_SECONDS         0x02
#define RTC_ADR_MINUTES         0x03
#define RTC_ADR_HOURS           0x04
#define RTC_ADR_DAYS            0x05
#define RTC_ADR_WEEKDAYS        0x06
#define RTC_ADR_MONTHS          0x07
#define RTC_ADR_YEARS           0x08
#define RTC_ADR_MINUTE_ALRM     0x09
#define RTC_ADR_HOURS_ALRM      0x0A
#define RTC_ADR_DAY_ALRM        0x0B
#define RTC_ADR_WEEKDAY_ALRM    0x0C
#define RTC_ADR_OFFSET          0x0D
#define RTC_ADR_CLKOUT          0x0E
#define RTC_ADR_COUNTDOWN       0x0F
                                
#define RTC_24HOUR_FORMAT       0x00
#define RTC_12HOUR_FORMAT       0x04

#define RTC_AM                  0x00
#define RTC_PM                  0x20

int16_t rtcWrite(uint8_t* buf, uint16_t offset, uint16_t len);
int16_t rtcRead(uint8_t* buf, uint16_t offset, uint16_t len);
void rtcSetHourFormat(uint8_t format);
