#include "lpc18xx_can.h"
#include "lpc18xx_gpio.h"
#include "lpc18xx_scu.h"
#include "lpc18xx_evrt.h"
#include "lpc18xx_cgu.h"

void CAN0_IRQHandler(void)
{
    CAN_IRQHandler();
}

/*********************************************************************//**
 * @brief        CAN selftestmode
 * @param[in]    none
 * @return         none
 **********************************************************************/
void CanSelfTest(LPC_C_CANn_Type* LPC_C_CANx, uint8_t sts)
{
    if (sts)
        LPC_C_CANx->TEST |= 0x01;
    else
        LPC_C_CANx->TEST &= ~(0x01);
}

/*********************************************************************//**
 * @brief        CAN initialization
 * @param[in]    none
 * @return         none
 **********************************************************************/
void CanInit(MSG_CB Tx_cb, MSG_CB Rx_cb)
{
    // Set autobaud to off
    scu_pinmux(2, 13, MD_PDN, FUNC0);
    GPIO_SetDir(1, 2, 1);
    GPIO_ClearValue(1, 2);
    
    scu_pinmux(1, 17, MD_PLN, FUNC5);                        //CAN1TX
    scu_pinmux(1, 18, MD_PLN | MD_EZI, FUNC5);    //CAN1RX

    /** set CAN Peripheral Clock = 12MHz */
    CGU_EntityConnect(CGU_CLKSRC_XTAL_OSC, CGU_BASE_APB3);

    CAN_Init(LPC_C_CAN0, CAN_BITRATE500K12MHZ, CLKDIV1 , Tx_cb, Rx_cb);

    // Enable Loopback mode
    CanSelfTest(LPC_C_CAN0, 1);
    
    NVIC_EnableIRQ(C_CAN0_IRQn);
}
