#include "lpc_types.h"
#include "lpc18xx_ssp.h"
#include "lpc18xx_scu.h"
#include "lpc18xx_gpio.h"
#include "spi.h"

typedef struct
{
    uint8_t     port;
    uint8_t     pin;
    uint8_t     csFunc;
    uint8_t     gpioFunc;
    uint8_t     gpioPort;
    uint32_t    gpioBit;
} ST_SpiSlave;

ST_SpiSlave slaveTable[] = 
{
     // port    pin     csFunc    gpioFunc  gpioPort    gpioBit
    {   1,      0,      FUNC5,    FUNC0,    0,          1 << 4},    //Accelerometer slave sellect pin config
    {   9,      0,      FUNC7,    FUNC0,    4,          1 << 12}    //RTC temporary slave sellect pin config
};
#define SLAVE_TABLE_SIZE (sizeof(slaveTable)/sizeof(ST_SpiSlave))

/*********************************************************************//**
 * @brief        SPI Initialization
 * @param[in]    none
 * @return         none
 **********************************************************************/
void SpiInit(void)
{
    SSP_CFG_Type SSP_ConfigStruct;
    
    /* Configure SSP0 pins*/
    SpiSlaveSellect(SLAVE_ID_NONE);
    scu_pinmux(0x1,2,MD_PLN_FAST,FUNC5);    // K1 P1.2    MOSI FUNC5    
    scu_pinmux(0x1,1,MD_PLN_FAST,FUNC5);    // K2 P1.1    MISO FUNC5    
    scu_pinmux(0x3,0,MD_PLN_FAST,FUNC4);    // A8 P3.0    SCK  FUNC4    
    
    /* Configure SSP1 pins*/
    scu_pinmux(0x1,5,MD_PLN_FAST,FUNC5);    // J4 P1.5    SSEL FUNC5    
    scu_pinmux(0x0,1,MD_PLN_FAST,FUNC1);    // G1 P0.1    MOSI FUNC1    
    scu_pinmux(0x0,0,MD_PLN_FAST,FUNC1);    // G2 P0.0    MISO FUNC1    
    scu_pinmux(0x1,19,MD_PLN_FAST,FUNC4);    // K9 P1.19    SCK  FUNC1    

    // initialize SSP configuration structure to default
    SSP_ConfigStructInit(&SSP_ConfigStruct);
    SSP_ConfigStruct.CPHA = SSP_CPHA_SECOND;
    SSP_ConfigStruct.CPOL = SSP_CPOL_LO;
    
    // Initialize SSP peripheral with parameter given in structure above
    SSP_Init(LPC_SSP0, &SSP_ConfigStruct);
    SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

    // Enable SSP peripheral
    SSP_Cmd(LPC_SSP0, ENABLE);
    SSP_Cmd(LPC_SSP1, ENABLE);
}

/*********************************************************************//**
 * @brief        SPI slave sellect
 * @param[in]    slaveID - spi device to activate
 * @return         none
 **********************************************************************/
void SpiSlaveSellect(uint8_t slaveID)
{
    uint8_t c;
    for(c = 0; c < SLAVE_TABLE_SIZE; c++)
    {
        if(c == slaveID)
        {
            scu_pinmux(slaveTable[c].port,slaveTable[c].pin,MD_PLN_FAST,slaveTable[c].csFunc);
        }
        else
        {
            scu_pinmux(slaveTable[c].port, slaveTable[c].pin, MD_PDN, slaveTable[c].gpioFunc);
            GPIO_SetDir(slaveTable[c].gpioPort, slaveTable[c].gpioBit, 1);
            GPIO_SetValue(slaveTable[c].gpioPort, slaveTable[c].gpioBit);
        }
    }
}
