#define EEPROM_CMD_WREN 				6 //110	
#define EEPROM_CMD_WRDI 				8	//100	
#define EEPROM_CMD_RDSR 				9	//101	
#define EEPROM_CMD_WRSR 				1	//001	
#define EEPROM_CMD_READ 				3	//011	
#define EEPROM_CMD_WRITE				2	//010	

int16_t eepromRead(uint8_t* buf, uint16_t offset, uint16_t len);
int16_t eepromWrite(uint8_t* buf, uint16_t offset, uint16_t len);
