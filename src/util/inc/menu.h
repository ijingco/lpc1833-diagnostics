typedef struct
{
    uint8_t cmd;                                // command
    uint8_t str[30];                        // command description
    uint8_t (*op)(uint8_t cmd);    // command operation
    void* subMenu;                            // submenu
} ST_MenuItem;

typedef struct
{
    uint8_t str[50];        // header text
    ST_MenuItem* item;    // menu item
    uint16_t cnt;                // menu item count
} ST_MenuTable;

#define MENU_EXIT            0
#define MENU_CONTINUE    1

uint16_t searchMenu(ST_MenuTable* menu, uint8_t key);
uint8_t runMenuItem(ST_MenuItem* menu,uint8_t cmd);
void runMenu(ST_MenuTable* menu);
uint8_t exitMenu(uint8_t cmd);
