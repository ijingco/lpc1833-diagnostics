#include "lpc18xx_ssp.h"
#include "rtcDriver.h"
#include "string.h"
#include "spi.h"

#define RTC_READ    0x90
#define RTC_WRITE    0x10

#define RTC_TOTAL_REG_SIZE    16

int16_t rtcWrite(uint8_t* buf, uint16_t offset, uint16_t len)
{
    uint16_t c;
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[RTC_TOTAL_REG_SIZE + 1];
    uint32_t Rx_Buf[RTC_TOTAL_REG_SIZE + 1];

  if (len > RTC_TOTAL_REG_SIZE || offset+len > RTC_TOTAL_REG_SIZE) {
    return -1;
  }

    Tx_Buf[0] = (offset & 0x0F) | RTC_WRITE;
    for(c = 0; c < len; c++)
    {
        Tx_Buf[1 + c] = buf[c];
    }
    memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
    
    SpiSlaveSellect(SLAVE_ID_RTC);
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = len + 1;
    SSP_ReadWrite(LPC_SSP0, &xferConfig, SSP_TRANSFER_POLLING);
    
    return len;
}

int16_t rtcRead(uint8_t* buf, uint16_t offset, uint16_t len)
{
    uint16_t c;
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[RTC_TOTAL_REG_SIZE + 1];
    uint32_t Rx_Buf[RTC_TOTAL_REG_SIZE + 1];

  if (len > RTC_TOTAL_REG_SIZE || offset+len > RTC_TOTAL_REG_SIZE) {
    return -1;
  }

    memset(&Tx_Buf[0], 0, sizeof(Tx_Buf));
    memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
    Tx_Buf[0] = (offset & 0x0F) | RTC_READ;
    
    SpiSlaveSellect(SLAVE_ID_RTC);
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = len + 1;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);

    for(c = 0; c < len; c++)
    {
        buf[c] = Rx_Buf[2 + c];
    }
    
    return len;
}



void rtcSetHourFormat(uint8_t format)
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[2];
    uint32_t Rx_Buf[2];
    
    memset(&Tx_Buf[0], 0, sizeof(Tx_Buf));
    memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
    Tx_Buf[0] = RTC_ADR_CTR1 | RTC_READ;
    
    SpiSlaveSellect(SLAVE_ID_RTC);
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 2;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);
    
    if(RTC_12HOUR_FORMAT)
        Tx_Buf[1] |= RTC_12HOUR_FORMAT;
    else
        Tx_Buf[1] &= !(RTC_12HOUR_FORMAT);
    
    Tx_Buf[0] = RTC_ADR_CTR1 | RTC_WRITE;
    memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
    
    SpiSlaveSellect(SLAVE_ID_RTC);
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 2;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);
}
