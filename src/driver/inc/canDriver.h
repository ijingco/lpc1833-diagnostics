typedef void (*MSG_CB)(uint32_t msg_no);

void CanSelfTest(LPC_C_CANn_Type* LPC_C_CANx, uint8_t sts);
void CanInit(MSG_CB Tx_cb, MSG_CB Rx_cb);
