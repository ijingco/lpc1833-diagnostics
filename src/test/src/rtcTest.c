#include "main.h"
#include "rtcDriver.h"

uint8_t testRtcCom(uint8_t cmd);
uint8_t testFormat(uint8_t cmd);

// RTC diagnostic menu------------------------------//
ST_MenuItem rtcMenuItem[] = {
    //    cmd   str                     op              subMenu
    {   'A',    "Connectivity test",    testRtcCom,     NULL},
    {    'B',   "Read Device ID",       testFormat,     NULL},
    {    'X',   "EXIT",                 exitMenu,       NULL}
};

#define RTC_MENU_ITEM_COUNT (sizeof(rtcMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable rtcMenu = {
    "RTC Test",
    rtcMenuItem,
    RTC_MENU_ITEM_COUNT
};

/*********************************************************************//**
 * @brief        Test communication with RTC
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testRtcCom(uint8_t cmd)
{
  uint8_t buff;
    
    // Write the value 0xFF to OFFSET register (0x0D)
    printString("RTC: Writing 0xFF to register 0x0D");
    buff = 0xFF;
    rtcWrite(&buff, RTC_ADR_OFFSET, 1);
    buff = 0x00;
    
    // Read OFFSET register (0x0D)
    printString("RTC: Reading register 0x0D");
    rtcRead(&buff, RTC_ADR_OFFSET, 1);
    
    // Check read value match the writen value
    printString("RTC: Verifying");
    if(buff == 0xFF)
        printString("RTC: OK");
    else
        printString("RTC: Invalid value");

    // return OFFSET register to default value
    buff = 0x00;
    rtcWrite(&buff, RTC_ADR_OFFSET, 1);
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testFormat(uint8_t cmd)
{    
  int16_t len = 0;
  uint8_t buff[2];
    
    // Set timte to 20:00
    printString("RTC: Setting time to 20:00");
    rtcSetHourFormat(RTC_24HOUR_FORMAT);    //24 hour format
    buff[0] = 0x00;    // Minutes
    buff[1] = 0x20;    // Hours
    len = rtcWrite(buff, RTC_ADR_MINUTES, 2);
  if (len != 2) {
    printString("RTC: Failed to set time");
    return MENU_CONTINUE;
  }
    
    // Change to 12 hour mode
    printString("RTC: Change setting to 12 hour mode");
    rtcSetHourFormat(RTC_12HOUR_FORMAT);
    
    // Read time
    printString("RTC: Reading time");
    rtcRead(buff, RTC_ADR_MINUTES, 2);
    
    // Check if time is 8:00 PM
    printString("RTC: Verifing");
    if(buff[1] == (RTC_PM | 0x08))
        printString("RTC: OK");
    else
        printString("RTC: Invalid time");
    
    return MENU_CONTINUE;
}
