#include "lpc18xx_ssp.h"
#include "string.h"
#include "eepromDriver.h"

#define EEPROM_PAGE_SIZE        16
#define EEPROM_TOTAL_SIZE        512

static uint32_t readStsReg()
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[2];
    uint32_t Rx_Buf[2];
    
    Tx_Buf[0] = EEPROM_CMD_RDSR;
    Tx_Buf[1] = 0;
    Rx_Buf[0] = 0;
    Rx_Buf[1] = 0;
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 2;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);
    return Rx_Buf[1];
}

static void writeEnable()
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf;
    uint32_t Rx_Buf;
    
    Tx_Buf = EEPROM_CMD_WREN;
    Tx_Buf = 0;
    
    xferConfig.tx_data    = &Tx_Buf;
    xferConfig.rx_data    = &Rx_Buf;
    xferConfig.length        = 1;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);
}

int16_t eepromWrite(uint8_t* buf, uint16_t offset, uint16_t len)
{
  int16_t written = 0;
  uint16_t wLen = 0;
  uint16_t off = offset;
    uint16_t c;
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[EEPROM_PAGE_SIZE + 2];
    uint32_t Rx_Buf[EEPROM_PAGE_SIZE + 2];

  if (len > EEPROM_TOTAL_SIZE || offset+len > EEPROM_TOTAL_SIZE) {
    return -1;
  }

    wLen = EEPROM_PAGE_SIZE - (off % EEPROM_PAGE_SIZE);
  wLen = MIN(wLen, len);

  while (len) {
    Tx_Buf[0] = ((off >> 5) & 0x08) | EEPROM_CMD_WRITE;
    Tx_Buf[1] = (off & 0xff);
        for(c = 0; c < wLen; c++)
        {
            Tx_Buf[2 + c] = buf[written + c];
        }
        memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
        
        xferConfig.tx_data    = Tx_Buf;
        xferConfig.rx_data    = Rx_Buf;
        xferConfig.length        = wLen + 2;
        writeEnable();
        SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);

    /* delay to wait for a write cycle */
        while(readStsReg() & 0x01);

    len     -= wLen;
    written += wLen;
    off     += wLen;

    wLen = MIN(EEPROM_PAGE_SIZE, len);
  }

  return written;
}

int16_t eepromRead(uint8_t* buf, uint16_t offset, uint16_t len)
{
    uint16_t c;
    uint32_t Tx_Buf[EEPROM_TOTAL_SIZE + 2];
    uint32_t Rx_Buf[EEPROM_TOTAL_SIZE + 2];
    SSP_DATA_SETUP_Type xferConfig;

  if (len > EEPROM_TOTAL_SIZE || offset+len > EEPROM_TOTAL_SIZE) {
    return -1;
  }

    memset(&Tx_Buf[0], 0, sizeof(Tx_Buf));
    memset(&Rx_Buf[0], 0, sizeof(Rx_Buf));
    Tx_Buf[0] = ((offset >> 5) & 0x08) | EEPROM_CMD_READ;
    Tx_Buf[1] = (offset & 0xff);
    
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = len + 2;
    SSP_ReadWrite(LPC_SSP1, &xferConfig, SSP_TRANSFER_POLLING);

    for(c = 0; c < len; c++)
    {
        buf[c] = Rx_Buf[2 + c];
    }

  return len;
}
