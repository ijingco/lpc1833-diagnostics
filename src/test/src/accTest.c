#include "lpc18xx_ssp.h"
#include "main.h"
#include "accDriver.h"
#include "stdio.h"

uint8_t testCom(uint8_t cmd);
uint8_t testDeviceId(uint8_t cmd);
uint8_t setRange(uint8_t cmd);
uint8_t testReadXYZ(uint8_t cmd);

// Range select--------------------------------------//
ST_MenuItem rangeMenuItem[] = {
    //    cmd     str      op           subMenu
    {     'A',    "16G",   setRange,    NULL},
    {     'B',    " 8G",   setRange,    NULL},
    {     'C',    " 4G",   setRange,    NULL},
    {     'D',    " 2G",   setRange,    NULL},
    {     'X',    "EXIT",  exitMenu,    NULL}
};

#define ACC_RANGE_ITEM_COUNT (sizeof(rangeMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable rangeMenu = {
    "Select range",
    rangeMenuItem,
    ACC_RANGE_ITEM_COUNT
};

// Accelerometer diagnostic menu------------------------------//
ST_MenuItem accMenuItem[] = {
    //    cmd    str                     op               subMenu
    {    'A',    "Connectivity test",    testCom,         NULL},
    {    'B',    "Read Device ID",       testDeviceId,    NULL},
    {    'C',    "Set Range",            NULL,            &rangeMenu},
    {    'D',    "Read XYZ Data",        testReadXYZ,     NULL},
    {    'X',    "EXIT",                 exitMenu,        NULL}
};

#define ACC_MENU_ITEM_COUNT (sizeof(accMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable accMenu = {
    "Accelerometer Test",
    accMenuItem,
    ACC_MENU_ITEM_COUNT
};

/*********************************************************************//**
 * @brief        Test communication with accelerometer
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testCom(uint8_t cmd)
{
    uint32_t readVal;
    
    // Write the value 0xFF to OFSX register (0x1E)
    printString("ACCELEROMETER: Writing 0xFF to register 0x1E");
    AccWrite(ACC_ADR_OFSX, 0xFF);
    
    // Read OFSX register (0x1E)
    printString("ACCELEROMETER: Reading register 0x1E");
    readVal = AccRead(ACC_ADR_OFSX);
    
    // Check read value match the writen value
    printString("ACCELEROMETER: Verifying");
    if(readVal == 0xFF)
        printString("ACCELEROMETER: OK");
    else
        printString("ACCELEROMETER: Invalid value");

    // return OFSX register to default value
    AccWrite(ACC_ADR_OFSX, 0x00);
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testDeviceId(uint8_t cmd)
{
    uint32_t id;
    
    // Read device ID
    printString("ACCELEROMETER: Reading Device ID");
    id = AccRead(ACC_ADR_DEVID);

    // Check if device ID is correct
    printString("ACCELEROMETER: Verifying");
    if(id == 0xE5)
        printString("ACCELEROMETER: OK");
    else
        printString("ACCELEROMETER: Invalid Device ID");

    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Set range
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t setRange(uint8_t cmd)
{
    switch(cmd)
    {
        case 'a':
        case 'A':
            AccSetRange(ACC_RANGE_16G);
            printString("ACCELEROMETER: Range was set to 16G");    
            break;
        
        case 'b':
        case 'B':
            AccSetRange(ACC_RANGE_8G);
            printString("ACCELEROMETER: Range was set to 8G");    
            break;
        
        case 'c':
        case 'C':
            AccSetRange(ACC_RANGE_4G);
            printString("ACCELEROMETER: Range was set to 4G");    
            break;
        
        case 'd':
        case 'D':
            AccSetRange(ACC_RANGE_2G);
            printString("ACCELEROMETER: Range was set to 2G");    
            break;
    }
    
    return MENU_CONTINUE;
}


/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testReadXYZ(uint8_t cmd)
{
  uint8_t buff[50];
    int32_t x, y, z;
    
    ReadAccXYZ(&x,&y,&z);
    sprintf((char*)buff, "ACCELEROMETER: x=%d, y=%d, z=%d", x, y, z);
    printString(buff);

    return MENU_CONTINUE;
}
