#include "lpc18xx_cgu.h"
#include "main.h"
#include "accTest.h"
#include "accDriver.h"
#include "eepromTest.h"
#include "rtcTest.h"
#include "ledTest.h"
#include "ledDriver.h"
#include "gpioTest.h"
#include "gpioDriver.h"
#include "canTest.h"
#include "canDriver.h"

// Main diagnostic menu-------------------------------------//
ST_MenuItem mainMenuItem[] = {
	//	cmd		str					op		subMenu
	{	'A',	"Accelerometer",    NULL,	&accMenu},
	{	'B',	"EEPROM",			NULL,	&eepromMenu},
	{	'C',	"RTC",				NULL,	&rtcMenu},
	{	'D',	"LED",				NULL,	&ledMenu},
	{	'E',	"GPIO",				NULL,	&gpioMenu},
	{	'F',	"CAN",				NULL,   &canMenu}
};

#define MAIN_MENU_ITEM_COUNT (sizeof(mainMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable mainMenu = {
	"NXP LPC1833JET100 Diagnostics",
	mainMenuItem,
	MAIN_MENU_ITEM_COUNT
};

int main(void)
{
	SystemInit();
	CGU_Init();
	ConsoleInit();
	SpiInit();
	AccInit();
	LedInit();
	GpioInit();
	CanInit(Tx_cb, Rx_cb);
	
	runMenu(&mainMenu);
}
