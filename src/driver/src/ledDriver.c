#include "lpc18xx_gpio.h"
#include "lpc18xx_scu.h"
#include "ledDriver.h"
#include "gpioDriver.h"

ST_GpioPinConfig cfgTbl[] =
{
    //port  pin    func            gpioPort           gpioBit            dir
    {2,     11,    FUNC0,            1,                11,                OUTPUT},    // Red
    {2,     9,     FUNC0,            1,                10,                OUTPUT},    // Green
    {2,     12,    FUNC0,            1,                12,                OUTPUT}     // Blue
};

/*********************************************************************//**
 * @brief        LED pin initialization
 * @param[in]    none
 * @return       none
 **********************************************************************/
void LedInit(void)
{
    uint8_t c;
    
    for(c = 0; c < 3; c++)
    {
        scu_pinmux(cfgTbl[c].port, cfgTbl[c].pin, MD_PDN, cfgTbl[c].func);
        GPIO_SetDir(cfgTbl[c].gpioPort, cfgTbl[c].gpioBit, cfgTbl[c].dir);
        GPIO_ClearValue(cfgTbl[c].gpioPort, cfgTbl[c].gpioBit);
    }
}

/*********************************************************************//**
 * @brief        LED control
 * @param[in]    led - LED pin to control
 *                         LED_RED     - Red LED
 *                         LED_GREEN   - Green LED
 *                         LED_BLUE    - Blue LED
 * @param[in]    sts - new state
 *                         LED_ON     - turn on LED
 *                         LED_OFF    - turn off LED
 * @return         none
 **********************************************************************/
void LedCtl(uint8_t led, uint8_t sts)
{
    if(sts == LED_ON)
    {
        GPIO_SetValue(cfgTbl[led].gpioPort, cfgTbl[led].gpioBit);
    }
    else
    {
        GPIO_ClearValue(cfgTbl[led].gpioPort, cfgTbl[led].gpioBit);
    }
}
