#include "lpc18xx_ssp.h"
#include "spi.h"
#include "accDriver.h"
#include "string.h"

uint32_t xOff, yOff,zOff;

static uint32_t fmt10to32Bit(uint32_t num)
{
    if(num & 0x200)
        num = num & 0xFFFFFC00;
    return num;
}

/*********************************************************************//**
 * @brief        Read raw accelerometer measurement
 * @param[in]    none
 * @return         none
 **********************************************************************/
static void ReadAccMeasure(int32_t *x, int32_t *y, int32_t *z)
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[7];
    uint32_t Rx_Buf[7];
    
    // initialize tx rx buffer
    memset(Tx_Buf, 0, sizeof(Tx_Buf));
    memset(Rx_Buf, 0, sizeof(Rx_Buf));
    
    // start address of registers to read
    Tx_Buf[0] = (1 << 7) || ACC_ADR_DATAX0;
    
    // set accelerometer as active slave
    SpiSlaveSellect(SLAVE_ID_ACC);
    
    // start communication
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 7;
    SSP_ReadWrite(LPC_SSP0, &xferConfig, SSP_TRANSFER_POLLING);
    
    // reading results
    *x = ((Rx_Buf[2] << 8) & 0xFF00)| (Rx_Buf[1] & 0xFF);
    *x = fmt10to32Bit(*x);
    *y = ((Rx_Buf[4] << 8) & 0xFF00)| (Rx_Buf[3] & 0xFF);
    *y = fmt10to32Bit(*y);
    *z = ((Rx_Buf[6] << 8) & 0xFF00)| (Rx_Buf[5] & 0xFF);
    *y = fmt10to32Bit(*z);
}

/*********************************************************************//**
 * @brief        Accelerometer initialization
 * @param[in]    none
 * @return         none
 **********************************************************************/
void AccInit(void)
{
    int32_t x, y, z;
    
    // set range to 2g
    AccWrite(ACC_ADR_DATA_FORMAT, ACC_RANGE_2G);
    
    // set accelerometer to measurement mode
    AccWrite(ACC_ADR_POWER_CTL, ACC_MODE_MEASURE);
    
    // calculate for offset values
    ReadAccMeasure(&x, &y, &z);
    xOff = 0 - x;
    yOff = 0 - y;
    zOff = 255 - z;
}

/*********************************************************************//**
 * @brief        Accelerometer read
 * @param[in]    none
 * @return         none
 **********************************************************************/    
uint32_t AccRead(uint32_t address)
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[2];
    uint32_t Rx_Buf[2];
    
    // initialize tx rx buffer
    memset(Tx_Buf, 0, sizeof(Tx_Buf));
    memset(Rx_Buf, 0, sizeof(Rx_Buf));
    
    // address of register to read
    Tx_Buf[0] = (1 << 7) || address;
    
    // set accelerometer as active slave
    SpiSlaveSellect(SLAVE_ID_ACC);
    
    // start communication
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 2;
    SSP_ReadWrite(LPC_SSP0, &xferConfig, SSP_TRANSFER_POLLING);
    
    // reading result
    return Rx_Buf[1];
}

/*********************************************************************//**
 * @brief        Accelerometer write
 * @param[in]    none
 * @return         none
 **********************************************************************/    
void AccWrite(uint32_t address, uint32_t data)
{
    SSP_DATA_SETUP_Type xferConfig;
    uint32_t Tx_Buf[2];
    uint32_t Rx_Buf[2];
    
    // initialize tx rx buffer
    memset(Tx_Buf, 0, sizeof(Tx_Buf));
    memset(Rx_Buf, 0, sizeof(Rx_Buf));
    
    // address of register to write
    Tx_Buf[0] = address;
    
    // data to be writen
    Tx_Buf[1] = data;
    
    // set accelerometer as active slave
    SpiSlaveSellect(SLAVE_ID_ACC);
    
    // start communication
    xferConfig.tx_data    = Tx_Buf;
    xferConfig.rx_data    = Rx_Buf;
    xferConfig.length        = 2;
    SSP_ReadWrite(LPC_SSP0, &xferConfig, SSP_TRANSFER_POLLING);
}

/*********************************************************************//**
 * @brief        Set accelerometer range
 * @param[in]    none
 * @return         none
 **********************************************************************/    
void AccSetRange(uint32_t range)
{
    int32_t x, y, z, rOff;
    
    AccWrite(ACC_ADR_DATA_FORMAT, range);
    
    // select offset
    switch(range)
    {
        case ACC_RANGE_2G:
            rOff = 255;
            break;
        
        case ACC_RANGE_4G:
            rOff = 127;
            break;
        
        case ACC_RANGE_8G:
            rOff = 63;
            break;
        
        case ACC_RANGE_16G:
            rOff = 31;
            break;
    }
    
    // recalculate offset
    ReadAccMeasure(&x, &y, &z);
    xOff = 0 - x;
    yOff = 0 - y;
    zOff = rOff - z;
}

/*********************************************************************//**
 * @brief        Read calibrated accelerometer measurement
 * @param[in]    none
 * @return         none
 **********************************************************************/
void ReadAccXYZ(int32_t *x, int32_t *y, int32_t *z)
{
    // read raw data
    ReadAccMeasure(x, y, z);
    
    // add offset
    *x = xOff + *x;
    *y = yOff + *y;
    *z = zOff + *z;
}
