#include "lpc18xx_can.h"
#include "main.h"

#define TX_MSG_ID            (0x200)
#define RX_MSG_ID            TX_MSG_ID

#define CAN_TIMEOUT            (0xFFFFFFFFUL)

uint8_t loopBack(uint8_t cmd);

// Accelerometer diagnostic menu------------------------------//
ST_MenuItem canMenuItem[] = {
    //   cmd     str                  op               subMenu
    {    'A',    "Loopback test",     loopBack,        NULL},
//    {    'B',    "Read Device ID",    testDeviceId,    NULL},
    {    'X',    "EXIT",              exitMenu,        NULL}
};

#define CAN_MENU_ITEM_COUNT (sizeof(canMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable canMenu = {
    "CAN Test",
    canMenuItem,
    CAN_MENU_ITEM_COUNT
};

message_object recv_obj;
uint8_t recFlag;

/*********************************************************************//**
 * @brief        CAN Loopback test
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t loopBack(uint8_t cmd)
{
    message_object send_obj;
    uint8_t c;
    uint32_t timeOut;
    
    recFlag = 1;
    
    // Listen for can message with the specified id
    recv_obj.id = RX_MSG_ID;
    CAN_Recv(LPC_C_CAN0, 1, (uint32_t*)&recv_obj, FALSE);
    
    //  Transmit 8 byte data
    send_obj.id = TX_MSG_ID;
    send_obj.dlc = 8;
    for( c = 0; c < send_obj.dlc; c++)
        send_obj.data[c] = c;
    CAN_Send(LPC_C_CAN0, 17, (uint32_t *)&send_obj);
    
    while(recFlag);
    
    // Wait for can message
    timeOut = CAN_TIMEOUT;
    while (recFlag) {
        if (timeOut == 0)
        {
            printString("CAN: Receive message timeout");
            break;
        }
        timeOut--;
    }
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Transmit callback function
 * @param[in]    msg_no        - message number
 * @return         none
 **********************************************************************/
void Tx_cb(uint32_t msg_no)
{
    printString("CAN: Message was sent");
    printString("CAN: Waiting for message");
}


/*********************************************************************//**
 * @brief        Receive callback function
 * @param[in]    msg_no        - message number
 * @return         none
 **********************************************************************/
void Rx_cb(uint32_t msg_no)
{
    uint8_t c;
    
    CAN_ReadMsg(LPC_C_CAN0, msg_no, &recv_obj);
    recFlag = 0;
    for( c = 0; c < recv_obj.dlc; c++)
    {
        if(!(recv_obj.data[c] == c))
        {
            printString("CAN: Invalid data received");
            return;
        }
    }
    printString("CAN: Message was received");
}
