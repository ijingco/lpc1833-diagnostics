#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "eepromDriver.h"

#define E_WRITE_LEN 512

uint8_t readAdr(uint8_t cmd);
uint8_t writeAdr(uint8_t cmd);
uint8_t testWR(uint8_t cmd);

// Main diagnostic menu-------------------------------------//
ST_MenuItem eepromMenuItem[] = {
    //    cmd        str                                            op                        subMenu
    {        'A',    "Read address",                    readAdr,            NULL},
    {        'B',    "Write address",                writeAdr,            NULL},
    {        'C',    "Full W/R test",                testWR,                NULL},
    {        'X',    "EXIT",                                    exitMenu,            NULL}
};

#define EEPROM_MENU_ITEM_COUNT (sizeof(eepromMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable eepromMenu = {
    "EEPROM Test",
    eepromMenuItem,
    EEPROM_MENU_ITEM_COUNT
};

/*********************************************************************//**
 * @brief        Read byte
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t readAdr(uint8_t cmd)
{    
    uint8_t buff[4], str[50], readVal;
    uint16_t adr;

    // Ask user for address to read
    printString("EEPROM: Enter address to read (0~511)");
    readString(3, buff);
    adr = atoi((char*)buff);
    
    // verify address
    if(adr > 511)
    {
        printString("EEPROM: Invalid address");
        return MENU_CONTINUE;
    }
    
    // Read address
    sprintf((char*)str, "EEPROM: Readding address %d",adr);
    printString(str);
    readVal = eepromRead(&readVal, adr, 1);
    
    // Display value of address
    sprintf((char*)str, "EEPROM: The value of address %d is %d",adr, readVal);
    printString(str);
    
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Write byte
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t writeAdr(uint8_t cmd)
{    
    uint8_t buff[4], str[50], writeVal;
    uint16_t adr;

    // Ask user for address to write
    printString("EEPROM: Enter address to write (0~511)");
    readString(3, buff);
    adr = atoi((char*)buff);
    
    // verify address
    if(adr > 511)
    {
        printString("EEPROM: Invalid address");
        return MENU_CONTINUE;
    }

    // Ask user for data to write
    printString("EEPROM: Enter data to write (0~255)");
    readString(3, buff);
    writeVal = atoi((char*)buff);
    
    // Write data
    sprintf((char*)str, "EEPROM: Writing %d to address %d",writeVal, adr);
    printString(str);
    eepromWrite(&writeVal, adr, 1);
    printString("Finished writing");
    
    return MENU_CONTINUE;
}


/*********************************************************************//**
 * @brief        Test EEPROM Write/Read
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testWR(uint8_t cmd)
{    
    uint16_t i;
    uint16_t offset = 0;
    uint8_t b[E_WRITE_LEN];

    // Initialize data to be writen
    for (i = 0; i < E_WRITE_LEN; i++) {
        b[i] = (uint8_t)(i+1);
    }

    // Write EEPROM data
    printString("EEPROM: Writing data");
    eepromWrite(b, offset, E_WRITE_LEN);
    printString("EEPROM: Finished writing");

    // Read EEPROM data
    memset(b, 0, E_WRITE_LEN);
    printString("EEPROM: Reading data");
    eepromRead(b, offset, E_WRITE_LEN);

    // Loop 512 times to check content of each byte
    printString("EEPROM: Verifing");
    for (i = 0; i < E_WRITE_LEN; i++) {
        if (b[i] != (uint8_t)(i+1)) {
            printString("EEPROM: Invalid data");
        return MENU_CONTINUE;
        }
    }
    printString("EEPROM: OK");
    return MENU_CONTINUE;
}
