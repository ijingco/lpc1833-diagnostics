#include "main.h"
#include "gpioDriver.h"

uint8_t testInput(uint8_t cmd);
uint8_t testOutput(uint8_t cmd);
uint8_t testPanic(uint8_t cmd);

// Digital out diagnostic menu-------------------------------------//
ST_MenuItem dOutMenuItem[] = {
    //    cmd     str       op              subMenu
    {     'A',    "High",   testOutput,     NULL},
    {     'B',    "Low",    testOutput,     NULL},
    {     'X',    "EXIT",   exitMenu,       NULL}
};

#define DOUT_MENU_ITEM_COUNT (sizeof(dOutMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable doutMenu = {
    "Please sellect output",
    dOutMenuItem,
    DOUT_MENU_ITEM_COUNT
};

// GPIO diagnostic menu-------------------------------------//
ST_MenuItem gpioMenuItem[] = {
    //    cmd     str                   op          subMenu
    {     'A',    "Digital Input",      testInput,  NULL},
    {     'B',    "Digital Output",     NULL,       &doutMenu},
    {     'C',    "Panic Input",        testPanic,  NULL},
    {     'X',    "EXIT",               exitMenu,   NULL}
};

#define GPIO_MENU_ITEM_COUNT (sizeof(gpioMenuItem)/sizeof(ST_MenuItem))

ST_MenuTable gpioMenu = {
    "GPIO Test",
    gpioMenuItem,
    GPIO_MENU_ITEM_COUNT
};

/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testInput(uint8_t cmd)
{
    // Enable digital input
    GpioCtl(DIGIN1_EN, HIGH);
    GpioCtl(DIGIN2_EN, HIGH);
    GpioCtl(DIGIN3_EN, HIGH);
    
    // Read digital input
    printString("GPIO: Reading digital input");
    // Digital input 1
    if(GpioRead(DIGIN1))
        printString("DIGIN1: HIGH");
    else
        printString("DIGIN1: LOW");
    
    // Digital input 2
    if(GpioRead(DIGIN2))
        printString("DIGIN2: HIGH");
    else
        printString("DIGIN2: LOW");
    
    // Digital input 3
    if(GpioRead(DIGIN2))
        printString("DIGIN3: HIGH");
    else
        printString("DIGIN3: LOW");

    // Disable digital input
    GpioCtl(DIGIN1_EN, LOW);
    GpioCtl(DIGIN2_EN, LOW);
    GpioCtl(DIGIN3_EN, LOW);
    
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testOutput(uint8_t cmd)
{
    switch(cmd)
    {
        case 'a':
        case 'A':
            // for confirmation
            //GpioCtl(DIGIOUT, HIGH);
            printString("GPIO: DIGIOUT was set to high");    
            break;
        
        case 'b':
        case 'B':
            // for confirmation
            //GpioCtl(DIGIOUT, LOW);
            printString("GPIO: DIGIOUT was set to low");    
            break;
    }

    // press any key to extit LED test
    printString("GPIO: Press any key to continue");
    readInput();
    //GpioCtl(DIGIOUT, LOW);
    
    return MENU_CONTINUE;
}

/*********************************************************************//**
 * @brief        Test accelerometer device ID
 * @param[in]    cmd        - sellected command
 * @return         MENU_CONTINUE    - continue to current menu
 *                        MENU_EXIT            - return to previous menu
 **********************************************************************/
uint8_t testPanic(uint8_t cmd)
{
    // Read digital input
    printString("GPIO: Reading panic input");

    // Panic low
    if(GpioRead(PANIC_LO))
        printString("PANIC_LO: HIGH");
    else
        printString("PANIC_LO: LOW");
    
    // Panic high
    if(GpioRead(PANIC_HI))
        printString("PANIC_HI: HIGH");
    else
        printString("PANIC_HI: LOW");

    return MENU_CONTINUE;
}
